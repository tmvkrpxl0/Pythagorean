#version 150

out vec4 color;
uniform int density;
uniform float toRaise;
uniform int draw_level;

bool isIn(float min, float x, float max);
vec2 inverse_coordinate(vec2 z);
void draw(vec2 mathCoord, bool isBackground);
void draw_lines(vec2 mathCoord);

void main() {
    vec2 center = vec2(400, 240);
    vec2 pixelCoord = gl_FragCoord.xy - center;
    vec2 mathCoord = pixelCoord / (density);
    vec2 inversed = inverse_coordinate(mathCoord);

    draw(mathCoord, true);
    draw(inversed, false);
    draw_lines(mathCoord);
}

void draw(vec2 mathCoord, bool isBackground) {
    vec2 roundCoord = vec2(round(mathCoord.x), round(mathCoord.y));
    if (distance(mathCoord, roundCoord) <= 0.1) {
        if(isBackground) color = vec4(1, 1, 1, 0.2);
        else color = vec4(1, 1, 0, 0.4);
    }else if (abs(mathCoord.x - 0) <= 0.2 || abs(mathCoord.y - 0) <= 0.1) {
        if(isBackground) color = vec4(1, 1, 1, 0.2);
        else color = vec4(0, 0.6, 1, 0.5);
    } else if(abs(mathCoord.x - roundCoord.x) <= 0.05 || abs(mathCoord.y - roundCoord.y) <= 0.05) {
        if(isBackground) color = vec4(1, 1, 1, 0.2);
        else color = vec4(0, 0.8, 0.4, 0.5);
    } else color = vec4(0, 0, 0, 0);
}

float mod(float a, float b) {
    return a - (b * floor(a/b));
}

vec2 inverse_coordinate(vec2 z) {
    float angle = atan(z.y, z.x);
    float length = distance(vec2(0, 0), z);
    float inverse_power = 1 / toRaise;
    vec2 normalized = vec2(cos(angle * inverse_power), sin(angle * inverse_power));
    return pow(length, inverse_power) * normalized;
}


//y = ax
//-ax + y = 0
// | -ap + f(p)|
// sqrt(a^2 + 1)
float distance_to_line(vec2 point, float slope) {
    return abs((-slope * point.x) + point.y) / sqrt(pow(slope, 2) + 1);
}

void draw_line(vec2 mathCoord, float slope) {
    if (distance_to_line(mathCoord, slope) <= 0.05) {
        color = vec4(1, 1, 1, 0.8);
    }
    if (distance_to_line(mathCoord, -slope) <= 0.05) {
        color = vec4(1, 1, 1, 0.8);
    }
    if (distance_to_line(mathCoord, 1/slope) <= 0.05) {
        color = vec4(1, 1, 1, 0.8);
    }
    if (distance_to_line(mathCoord, -1/slope) <= 0.05) {
        color = vec4(1, 1, 1, 0.8);
    }
}

void draw_lines(vec2 mathCoord) {
    if (draw_level == 0) return;
    draw_line(mathCoord, 2147483647);

    draw_line(mathCoord, 4/3.0);
    draw_line(mathCoord, 12/5.0);
    draw_line(mathCoord, 15/8.0);
    draw_line(mathCoord, 24/7.0);
    draw_line(mathCoord, 21/20.0);
    draw_line(mathCoord, 35/12.0);
    draw_line(mathCoord, 40/9.0);
    draw_line(mathCoord, 45/28.0);
    draw_line(mathCoord, 60/11.0);
    draw_line(mathCoord, 63/16.0);
    draw_line(mathCoord, 56/33.0);
    draw_line(mathCoord, 55/48.0);
    draw_line(mathCoord, 84/13.0);
    draw_line(mathCoord, 77/36.0);
    draw_line(mathCoord, 80/39.0);
    draw_line(mathCoord, 72/65.0);

    if (draw_level == 1) return;

    draw_line(mathCoord, 99/20.0);
    draw_line(mathCoord, 91/60.0);
    draw_line(mathCoord, 112/15.0);
    draw_line(mathCoord, 117/44.0);
    draw_line(mathCoord, 105/88.0);
    draw_line(mathCoord, 144/17.0);
    draw_line(mathCoord, 143/24.0);
    draw_line(mathCoord, 140/51.0);
    draw_line(mathCoord, 132/85.0);
    draw_line(mathCoord, 120/119.0);
    draw_line(mathCoord, 165/52.0);
    draw_line(mathCoord, 180/19.0);
    draw_line(mathCoord, 176/57.0);
    draw_line(mathCoord, 153/104.0);
    draw_line(mathCoord, 168/95.0);
    draw_line(mathCoord, 195/28.0);
    draw_line(mathCoord, 187/84.0);
    draw_line(mathCoord, 156/133.0);
    draw_line(mathCoord, 220/21.0);
    draw_line(mathCoord, 171/140.0);
    draw_line(mathCoord, 221/60.0);
    draw_line(mathCoord, 208/105.0);
    draw_line(mathCoord, 209/120.0);
    draw_line(mathCoord, 255/32.0);
    draw_line(mathCoord, 264/23.0);
    draw_line(mathCoord, 247/96.0);
    draw_line(mathCoord, 260/69.0);
    draw_line(mathCoord, 252/115.0);
    draw_line(mathCoord, 231/160.0);
    draw_line(mathCoord, 240/161.0);
    draw_line(mathCoord, 285/68.0);
}

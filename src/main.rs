#[macro_use]
extern crate glium;

use std::fs::read_to_string;

use glium::{
    Blend, BlendingFunction, DrawParameters, IndexBuffer, LinearBlendingFactor, Program,
    Surface, VertexBuffer,
};
use glium::backend::glutin::SimpleWindowBuilder;
use glium::index::PrimitiveType::TrianglesList;
use winit::event::{ElementState, Event, KeyEvent, StartCause, WindowEvent};
use winit::event_loop::EventLoop;
use winit::keyboard::{KeyCode, PhysicalKey};
use winit::window::Fullscreen;

#[derive(Copy, Clone)]
struct Vertex {
    position: [f32; 2],
}

implement_vertex!(Vertex, position);

fn main() {
    let vertices = vec![
        Vertex {
            position: [-1.0, -1.0],
        },
        Vertex {
            position: [-1.0, 1.0],
        },
        Vertex {
            position: [1.0, 1.0],
        },
        Vertex {
            position: [1.0, -1.0],
        },
    ];
    let indices: [u16; 6] = [0, 1, 2, 0, 2, 3];

    let event_loop = EventLoop::new().unwrap();
    let (window, display) = SimpleWindowBuilder::new().build(&event_loop);
    let fs = Fullscreen::Borderless(Some(
        window
            .available_monitors()
            .next()
            .unwrap(),
    ));
    window.set_fullscreen(Some(fs));
    let vertex_buffer = VertexBuffer::new(&display, &vertices).unwrap();
    let index_buffer = IndexBuffer::new(&display, TrianglesList, &indices).unwrap();

    let vertex = read_to_string("vertex.vert").unwrap();
    let fragment = read_to_string("fragment.frag").unwrap();

    let program = Program::from_source(&display, &vertex, &fragment, None).unwrap();

    let mut density = 60;
    let mut p: f32 = 0.0;
    let mut to_raise: f32 = 1.0;
    let mut draw_level = 0;

    let param = DrawParameters {
        blend: Blend {
            color: BlendingFunction::Addition {
                source: LinearBlendingFactor::SourceAlpha,
                destination: LinearBlendingFactor::OneMinusSourceAlpha,
            },
            alpha: BlendingFunction::Addition {
                source: LinearBlendingFactor::SourceAlpha,
                destination: LinearBlendingFactor::OneMinusSourceAlpha,
            },
            constant_value: (0.0, 0.0, 0.0, 0.0),
        },
        ..DrawParameters::default()
    };

    event_loop.run(move |event, target| {
        match event {
            Event::WindowEvent { event, .. } => match event {
                WindowEvent::KeyboardInput {
                    event: KeyEvent {
                        physical_key: PhysicalKey::Code(code),
                        state: ElementState::Pressed,
                        ..
                    }, ..
                } => {
                    match code {
                        KeyCode::ArrowUp => {
                            density += 1;
                        }
                        KeyCode::ArrowDown => {
                            density -= 1;
                        }
                        KeyCode::ArrowLeft => {
                            p += 0.01;
                            to_raise = (2f32).powf(p);
                        }
                        KeyCode::ArrowRight => {
                            p -= 0.01;
                            to_raise = (2f32).powf(p);
                        }
                        KeyCode::Escape => {
                            target.exit()
                        }
                        KeyCode::ControlLeft => {
                            draw_level += 1;
                            if draw_level == 3 {
                                draw_level = 0
                            }
                        }
                        KeyCode::AltLeft => {
                            if to_raise >= 2.0 {
                                to_raise = 1.0
                            } else {
                                to_raise = 2.0
                            }
                        }
                        _ => (),
                    }
                    return;
                }
                WindowEvent::CloseRequested => {
                    target.exit();
                    return;
                }
                WindowEvent::RedrawRequested => {
                    let mut frame = display.draw();
                    frame.clear_color(0.0, 0.0, 0.0, 1.0);
                    frame
                        .draw(
                            &vertex_buffer,
                            &index_buffer,
                            &program,
                            &uniform! { density : density, toRaise: to_raise, draw_level : draw_level},
                            &param,
                        )
                        .unwrap();
                    frame.finish().unwrap();
                }
                _ => return,
            },

            Event::AboutToWait => {
                window.request_redraw();
            }

            Event::NewEvents(cause) => match cause {
                StartCause::ResumeTimeReached { .. } => (),
                StartCause::Init => (),
                _ => return,
            },

            _ => return,
        }
    }).unwrap();
}
